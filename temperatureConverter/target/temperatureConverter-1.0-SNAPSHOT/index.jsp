<!DOCTYPE HTML>
<!-- Front end for the webBookQuote Servlet. -->

<html lang="en">
<head>
    <title>Celsius to Fahrenheit converter</title>
    <link rel="stylesheet" href="stylesheet.css">
    <meta charset="UTF-8">
</head>

<body>
<h1>Celsius to Fahrenheit converter</h1>

<form ACTION="temperature-converter">
    <p>Enter temperature in Celsius:  <input TYPE="TEXT" NAME="temp"></p>
    <input TYPE="SUBMIT">
</form>

<p>Result: </p>

</body>
</html>
