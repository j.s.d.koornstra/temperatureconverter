package com.example.temperatureConverter;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "temperatureConverter", value = "/temperature-converter")
public class temperatureConverter extends HttpServlet {
    private String message;

    public void init() {
        message = "temp";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE>\n";
        out.println(docType +

                "<HTML>\n" +
                "<head>\n" +
                        "    <title>Celsius to Fahrenheit converter</title>\n" +
                        "    <link rel=\"stylesheet\" href=\"stylesheet.css\">\n" +
                        "    <meta charset=\"UTF-8\">\n" +
                        "</head>\n" +
                        "\n" +
                        "<body>\n" +
                        "<h1>Celsius to Fahrenheit converter</h1>\n" +
                        "\n" +
                        "<form ACTION=\"temperature-converter\">\n" +
                        "    <p>Enter temperature in Celsius:  <input TYPE=\"TEXT\" NAME=\"temp\"></p>\n" +
                        "    <input TYPE=\"SUBMIT\">\n" +
                        "</form>\n" +
                        "\n" +
                        "<p>Result: " +  Converter.getFahrenheitTemp(request.getParameter("temp")) + " Fahrenheit</p>\n" +
                        "\n" +
                        "</body>\n" +
                        "</HTML>");
    }

    public void destroy() {
    }
}