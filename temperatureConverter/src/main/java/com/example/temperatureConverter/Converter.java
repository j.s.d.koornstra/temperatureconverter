package com.example.temperatureConverter;

public class Converter {

    public static double getFahrenheitTemp(String celsiusTemp) {
        double result = 0.0;



        try {
            double celsius = Double.parseDouble(celsiusTemp);
            result = celsius * 9/5 + 32;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        return result;
    }
}