package test.nl.utwente.di.bookQuote; 

import nl.utwente.di.bookQuote.Quoter;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After; 

/** 
* Quoter Tester. 
* 
* @author <Authors name> 
* @since <pre>apr. 20, 2021</pre> 
* @version 1.0 
*/ 
public class TestQuoter {

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getBookPrice(String isbn) 
* 
*/ 
@Test
public void testGetBookPrice() throws Exception { 
//TODO: Test goes here...
Quoter quoter = new Quoter();

double price = quoter.getBookPrice("1");
Assert.assertEquals("ISBN 1 = ", 10.0, price, 0.0);

    price = quoter.getBookPrice("2");
    Assert.assertEquals("ISBN 1 = ", 45.0, price, 0.0);

    price = quoter.getBookPrice("3");
    Assert.assertEquals("ISBN 1 = ", 20.0, price, 0.0);

    price = quoter.getBookPrice("4");
    Assert.assertEquals("ISBN 1 = ", 35.0, price, 0.0);

    price = quoter.getBookPrice("5");
    Assert.assertEquals("ISBN 1 = ", 50.0, price, 0.0);

    price = quoter.getBookPrice("6");
    Assert.assertEquals("ISBN 1 = ", 0, price, 0.0);
} 


} 
